Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }

  root 'hello_world#index'
  get '/livestream/:uid', to: 'hello_world#index'

  # API routes

  post '/post', to: 'api#post'
end
