class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token

  def post
    sign_out current_user
    redirect_to root_path && return
    if params[:liveChatId].present? && params[:messageText].present?
      uri = URI.parse('https://www.googleapis.com/youtube/v3/liveChat/messages?part=snippet')
      headers = {'Content-Type' => "application/json", 'authorization' => "Bearer #{current_user.token}"}
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Post.new(uri.request_uri, headers)
      data = {  "snippet":{ "liveChatId": params[:liveChatId], "type":"textMessageEvent", "textMessageDetails": { "messageText": params[:messageText] } } }
      request.body = data.to_json
      response = http.request(request)

      resp = {}
      if response.code == 200
        resp = { status: true, msg: "Message posted successfully", code: response.code }
      elsif response.code == 401
        resp = { status: false, msg: "Message couldn't be posted, try again!", code: response.code }
      else
        resp = { status: false, msg: "Message couldn't be posted, try again!", code: response.code }
      end

      render json: resp
    else
      render json: {status: false, msg: "Please try again"}
    end
  end
end
