class HelloWorldController < ApplicationController
  layout "hello_world"

  def index
    @hello_world_props = { cookies: current_user.token,  username: current_user.name }
  end
end
