import axios from 'axios';

function getLiveStreams(dispatch) {
  fetch('https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&q=game&type=video&maxResults=25&key=AIzaSyD8RrD1H724uGyZye8TOcl3r__EWJMG688')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch({type: 'SET_YOUTUBE_ITEMS', items: data.items });
    });
}

function searchLiveStreams(dispatch, query) {
  fetch(`https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&q=${query}&type=video&maxResults=25&key=AIzaSyD8RrD1H724uGyZye8TOcl3r__EWJMG688`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch({type: 'SET_YOUTUBE_ITEMS', items: data.items });
    });
}

function getLiveStreamsDetails(dispatch, videoID) {
  fetch(`https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics,liveStreamingDetails&id=${videoID}&key=AIzaSyD8RrD1H724uGyZye8TOcl3r__EWJMG688`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      dispatch({type: 'SET_CURRENT_ITEM', current_item: data.items[0] });
      // after loading livestream details load live chat
      getLiveStreamsChat(dispatch, data.items[0].liveStreamingDetails.activeLiveChatId);
    });
}

function getLiveStreamsChat(dispatch, liveChatId) {
  fetch(`https://www.googleapis.com/youtube/v3/liveChat/messages?part=id,snippet,authorDetails&liveChatId=${liveChatId}&key=AIzaSyD8RrD1H724uGyZye8TOcl3r__EWJMG688`)
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      console.log(data);
      dispatch({type: 'SET_CHAT', chat: data.items });
      dispatch({type: 'INCR_CHAT_COUNTER'});
    });
}

function sendMessageToLiveStream(dispatch, liveChatId, message, token) {
  axios.post('https://www.googleapis.com/youtube/v3/liveChat/messages?part=snippet&key=AIzaSyD8RrD1H724uGyZye8TOcl3r__EWJMG688', {  "snippet":{ "liveChatId": liveChatId, "type":"textMessageEvent", "textMessageDetails": { "messageText": message } } }, {'headers': { 'Content-Type': 'application/json', 'authorization': `Bearer ${token}` }})
    .then((data) => {
      console.log(data);
    })
    .catch(error => {
      alert("SORRY SOMETHING WENT WRONG PLEASE TRY AGAIN OR LOGOUT AND LOGIN AGAIN");
    });
}


export default {
  getLiveStreams, getLiveStreamsDetails, getLiveStreamsChat, sendMessageToLiveStream, searchLiveStreams
}
