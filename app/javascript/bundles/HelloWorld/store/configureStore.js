import { createStore } from 'redux';
var dateFormat = require('dateformat');

const initialState = {
  items: [],
  current_item: null,
  chat: [],
  chat_counter: 0,
  message: null,
  cookies: null,
  chatItems: [],
  searchQuery: "",
  username: null,
  topUsers: [],
  timeGraph: [],
  topUser: [],
  timeGraph: [],
  streamSearch: ""
}

var calculateStats = (chat) => {
  var userHash = {};
  var timeHash = {};

  chat.map((item) => {
    var channelId = item.authorDetails.channelId;
    var authorName = item.authorDetails.displayName;
    var publishedAt = new Date(item.snippet.publishedAt);
    var time = dateFormat(publishedAt, "h:MM TT");

    if(channelId in userHash)
      userHash[channelId].messageCount +=1;
    else
      userHash[channelId] = { messageCount: 1, authorName: authorName };

    timeHash[time] = time in timeHash ? timeHash[time]+1 : 1;
  });

  var topUser = {};
  Object.keys(userHash).map(item => {
    if(userHash[item].messageCount in topUser)
      topUser[userHash[item].messageCount].push(userHash[item].authorName);
    else
      topUser[userHash[item].messageCount] = [userHash[item].authorName];
  });

  // iterate hash and convert to array
  var keys = Object.keys(topUser).sort().reverse();
  var users = [];
  keys.map((key) => {
    topUser[key].map((user) => {
      users.push([user, key]);
    })
  });

  
  users = users.slice(0, 10);

  keys = Object.keys(timeHash);
  var timeGraph = [];
  keys.map((key) => {
    timeGraph.push([key, timeHash[key]]);
  });  

  return { topUser: users, timeGraph: timeGraph }
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_YOUTUBE_ITEMS':
      return Object.assign({}, state, {items: action.items});
    case 'SET_STREAM_SEARCH':
      return Object.assign({}, state, {streamSearch: action.streamSearch});
    case 'SET_CURRENT_ITEM':
      return Object.assign({}, state, {current_item: action.current_item});
    case 'SET_CHAT':
      const stats = calculateStats(action.chat);
      return Object.assign({}, state, {topUser: stats.topUser, timeGraph: stats.timeGraph, chat: action.chat, chatItems: (state.searchQuery == "") ? action.chat : state.chatItems});
    case 'SET_CHAT_ITEMS':
      return Object.assign({}, state, {chatItems: action.chat});
    case 'SET_MESSAGE':
      return Object.assign({}, state, {message: action.message});
    case 'SET_COOKIES':
      return Object.assign({}, state, {cookies: action.cookies});
    case 'SET_USERNAME':
      return Object.assign({}, state, {username: action.username});
    case 'INCR_CHAT_COUNTER':
        return Object.assign({}, state, {chat_counter: state.chat_counter + 1});
    case 'CLR_CHAT_COUNTER':
      return Object.assign({}, state, {chat_counter: 0});
    case 'CLR_SEARCH_QUERY':
      var chatItems = state.chat.filter(function(chat){
        return chat.snippet.textMessageDetails.messageText.toLowerCase().search(
          action.searchQuery.toLowerCase()) !== -1;
      });
      return Object.assign({}, state, {searchQuery: action.searchQuery, chatItems: chatItems});
    case 'UPDATE_STATS':
      return Object.assign({}, state, calculateStats(state.chat));
    default:
      return state;
  }
}


const store = createStore(reducer);

export default store;
