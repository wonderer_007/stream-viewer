import React from 'react';
import ReactDOM from 'react-dom'
import TimerMixin from 'react-timer-mixin';
import {connect} from 'react-redux';
import Iframe from 'react-iframe';

import YouTube from '../api/YouTube';
import TopUser from './TopUser';
import TimeGraph from './TimeGraph';

var dateFormat = require('dateformat');

class LiveStream extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { uid } = this.props.match.params;
    // load livestream details
    this.props.getLivestreamDetails(uid);
  }

  componentDidUpdate(prevProps) {
    if(this.el != null)
      this.el.scrollTop = this.el.scrollHeight;

    if(prevProps.chat_counter != this.props.chat_counter) {
      TimerMixin.setTimeout(
        () => {
          this.props.loadChat(this.props.current_item.liveStreamingDetails.activeLiveChatId, prevProps.chat_counter);
        },20000);
    }
  }

  trimString(string) {
    return (string.length > 500) ? (string.substring(0, 500) + "...") : string;
  }


  componentWillUnmount() {
    this.props.setCurrentItem(null);
    this.props.setChat([]);
    this.props.clearChatCounter();
  }

  renderPage() {
    if(this.props.current_item == null) return;
    return (
            <div className="row">
              <div className="iframe-container col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <iframe src={`https://www.youtube.com/embed/${this.props.current_item.id}?autoplay=1&cc_load_policy=1&rel=0`}  width="100%" height="400px" />
                <label className="title">{this.props.current_item.snippet.title}</label><br/>
                <p><small className="publishedAt">{ dateFormat(new Date(this.props.current_item.snippet.publishedAt), "dddd, mmmm dS, yyyy, h:MM:ss TT")}</small></p><br/>
                <p>{this.trimString(this.props.current_item.snippet.description)}</p>
              </div>

              <div className="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                {this.renderChatBox()}
              </div>
            </div>
    )
  }

  renderChatBox() {
    if(this.props.current_item == null || this.props.chat == null) {
      return (<p>Chat for this stream has been disabled</p>)
    }

    const overflow = {'height': '350px', 'overflow': 'scroll', 'marginLeft': '-40px'};
    return (
          <div>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Live Chat</h3>
              </div>
              <div className="well well-sm">
                <input ref="messageText" type="text" autoComplete="off" className="form-control form-control-lg form-control-borderless" placeholder="search keyword" onChange={(evt) => this.props.filterList(evt)}  />
              </div>
              <div className="panel-body" >
                  <ul className="chat" style={overflow} ref={el => { this.el = el; }} >
                    {
                      this.props.chatItems.map((chat) => {
                        return (
                                <li className="left clearfix chatItem" key={chat.id} >
                                  <span className="chat-img pull-left">
                                    <img src={chat.authorDetails.profileImageUrl} alt="User Profile" className="img-circle"/>
                                  </span>
                                  <div className="chat-body clearfix">
                                      <div className="chat-details">
                                        <a herf={ `https://www.youtube.com/channel/${chat.authorDetails.channelId}`} target="_blank" className="primary-font"> {chat.authorDetails.displayName} </a>
                                        <span className="chat-text">
                                        { (chat.snippet.textMessageDetails == null) ? chat.snippet.displayMessage : chat.snippet.textMessageDetails.messageText}
                                        </span>
                                        <small className="pull-right publishedAt">
                                          <span className="glyphicon glyphicon-time"></span>
                                          {dateFormat(new Date(chat.snippet.publishedAt), "h:MM:ss TT")}
                                        </small>
                                      </div>
                                  </div>
                                </li>
                        )
                      })
                    }
                  </ul>
              </div>
              <div className="panel-footer">
                  <div className="input-group">
                    <form className="chatForm" onSubmit={(evt) => this.props.postMessageToLivestream(evt, this.refs.inputMessage.value, this.props.current_item.liveStreamingDetails.activeLiveChatId, this.props.cookies, this)}>
                      <input type="text" ref="inputMessage" className="form-control input-sm" placeholder="Say Something ..."  />
                      <span className="input-group-btn">
                          <button className="btn btn-warning btn-sm" id="btn-chat">
                              Send</button>
                      </span>                      
                    </form>
                  </div>
              </div>
            </div>
          </div>
    )
  }

  render() {
    return (
      <div>
        <div className="container">
          {this.renderPage()}
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-8">
              <TimeGraph timeGraph={this.props.timeGraph} />
            </div>
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-4">
              <TopUser topUser={this.props.topUser} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.items,
    current_item: state.current_item,
    chat: state.chat,
    message: state.message,
    token: state.token,
    chat_counter: state.chat_counter,
    chatItems: state.chatItems,
    cookies: state.cookies,
    topUser: state.topUser,
    timeGraph: state.timeGraph,
    username: state.username
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    handleInputChange: (evt) => {
      dispatch({type: 'SET_MESSAGE', message: evt.target.value});
    },
    setCurrentItem: (item) => {
      dispatch({type: 'SET_CURRENT_ITEM', current_item: item});
    },
    setChat: (chat) => {
      dispatch({type: 'SET_CHAT', chat: chat});
    },
    getLivestreamDetails:(videoId) => {
      YouTube.getLiveStreamsDetails(dispatch, videoId);
    },
    postMessageToLivestream: (evt, message, liveChatId, cookies, ptr) => {
      evt.preventDefault();
      YouTube.sendMessageToLiveStream(dispatch, liveChatId, message, cookies);
      ptr.refs.inputMessage.value = "";
    },
    clearChatCounter: () => {
      dispatch({type: 'CLR_CHAT_COUNTER'});
    },
    loadChat: (liveChatId, chatCounter) => {
      YouTube.getLiveStreamsChat(dispatch, liveChatId);
    },
    filterList(event) {
      dispatch({type: 'CLR_SEARCH_QUERY', searchQuery: event.target.value});
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LiveStream);
