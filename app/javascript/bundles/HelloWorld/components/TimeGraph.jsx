import React from 'react';
import ReactDOM from 'react-dom'
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

class TimeGraph extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
  }

  render() {
    var data = [];
    var count = 0;
    this.props.timeGraph.map((item) => {
      data.push({name: item[0], messageCount: item[1], amt: count});
      count +=1;
    });

    var height = (this.body == null) ? 0: this.body.clientHeight;
    var width = (this.body == null) ? 0: this.body.clientWidth;

    return (

          <div className="panel panel-default" >
            <div className="panel-heading">
              <h3 className="panel-title">Live Chat Graph</h3>
            </div>
            <div className="panel-body timeGraph" ref={body => { this.body = body; }}>
              <LineChart width={width} height={400} data={data}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}>
               <XAxis dataKey="name"/>
               <YAxis/>
               <CartesianGrid strokeDasharray="3 3"/>
               <Tooltip/>
               <Legend />
               <Line type="monotone" dataKey="messageCount" stroke="#8884d8" activeDot={{r: 8}}/>
              </LineChart>
            </div>
          </div>
    );
  }
}

export default (TimeGraph);
