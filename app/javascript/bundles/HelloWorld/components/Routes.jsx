import React from 'react';
import { Provider } from 'react-redux'
import {connect} from 'react-redux';

import HomePage from './HomePage';
import LiveStream from './LiveStream';
import store from '../store/configureStore';

import configureStore from '../store/configureStore';

import {
  BrowserRouter,
  Route,
  Link,
  Switch
} from 'react-router-dom'

class Routes extends React.Component {
  constructor(props) {
    super(props);
    store.dispatch({type: 'SET_COOKIES', cookies: this.props.cookies});
    store.dispatch({type: 'SET_USERNAME', username: this.props.username});
  }

  render() {
    return (
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route path="/" exact component={HomePage} />
              <Route path="/livestream/:uid" exact component={LiveStream} />
            </Switch>
          </BrowserRouter>
        </Provider>

    );
  }
}

export default (Routes)
