import React from 'react';
import ReactDOM from 'react-dom'

const uuidv1 = require('uuid/v1');

class TopUser extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
  }

  render() {
    return (
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Top 10 Users</h3>
              </div>
              <div className="panel-body">
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th> User Name </th>
                      <th> Message Count </th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.props.topUser.map((user) => {
                        return (
                          <tr key={uuidv1()}>
                            <td> { user[0] } </td>
                            <td> { user[1] } </td>
                          </tr>
                        )
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
    );
  }
}

export default (TopUser);
