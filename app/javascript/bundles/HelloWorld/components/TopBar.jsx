import React from 'react';

class TopBar extends React.Component {
  constructor(props) {
    super(props);
  }

  logout(evt) {
    evt.preventDefault();
  }

  render() {
    return (
      <nav className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="navbar-header">
            <a className="navbar-brand" href="#">StreamViewer</a>
          </div>
          <ul className="nav navbar-nav">
            <li className="active"><a href={window.location.origin}>Home</a></li>
          </ul>
          <ul className="nav navbar-nav navbar-right">
            <li><a href="#">Hi {this.props.username}!</a></li>
            <li><a href="/users/sign_out" data-method="delete" ><span className="glyphicon glyphicon-log-in"></span> Logout</a></li>
          </ul>
        </div>
      </nav>
    );
  }
}

export default (TopBar);
