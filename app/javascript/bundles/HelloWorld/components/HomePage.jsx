import React from 'react';
import { Link } from 'react-router-dom';
import Iframe from 'react-iframe';
import {connect} from 'react-redux';

import YouTube from '../api/YouTube';
import chunks from 'array.chunk';
var dateFormat = require('dateformat');

class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.loadItems();
  }

  parseDate(time) {
    return time;
  }

  displayDate(time) {
    var date = new Date(time);
    return `${date.getHours()}:${date.getMinutes()}`;
  }

  trimString(string) {
    return (string.length > 53) ? (string.substring(0, 53) + "...") : string;
  }

  renderSearch() {
    return (
            <form onSubmit={(evt) => this.props.search(evt, this.props.streamSearch)}> 
              <div className="row">
                <div className="col-md-7 col-sm-12 col-xs-12 col-lg-5 col-centered">
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder="Search" id="txtSearch" value={this.props.streamSearch} onChange={(evt) => this.props.set_search_query(evt)} />
                    <div className="input-group-btn">
                      <button className="btn btn-primary" type="submit">
                        <span className="glyphicon glyphicon-search"></span>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
    );
  }

  renderStreams() {
    return (
            <div className="container">
              {
                this.renderSearch()
              }
                  <div className="clearfix"></div>
              {
                chunks(this.props.items, 4).map((items) => {
                  return (<div key={items[0].etag} className="row topBuffer">
                  {
                    items.map((item) => {
                      return (
                        <div key={item.etag} className="col-xs-12 col-sm-6 col-md-3 col-lg-3 pull-left" >
                            <div className="video-player">
                              <Link to={`livestream/${item.id.videoId}` } >
                                <img src={item.snippet.thumbnails.high.url} className="videoThumbnail" data-videoid={item.id.videoId}  />
                              </Link>
                              </div>
                              <div className="videoDetails">
                                <Link to={`/livestream/${item.id.videoId}` } data-videoid={item.id.videoId}>
                                  <label className="title">{this.trimString(item.snippet.title)}</label>
                                </Link>
                                <small className="publishedAt">{ dateFormat(new Date(item.snippet.publishedAt), "dddd, mmmm dS, yyyy, h:MM:ss TT")}</small>
                              </div>
                        </div>
                      )
                    })
                  }
                  <div className="clearfix"></div>
                  </div>)
                })
              }
            </div>
    )
  }

  render() {
    return (
      <div>
        {this.renderStreams()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    items: state.items,
    current_item: state.current_item,
    token: state.token,
    username: state.username,
    streamSearch: state.streamSearch
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    loadItems: () => {
      YouTube.getLiveStreams(dispatch);
    },
    search(event, query) {
      event.preventDefault();

      if(query == null || query == "")
        YouTube.getLiveStreams(dispatch);
      else
        YouTube.searchLiveStreams(dispatch, query);
    },
    set_search_query(event) {
      dispatch({type: 'SET_STREAM_SEARCH', streamSearch: event.target.value});
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
