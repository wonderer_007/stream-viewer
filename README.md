# README

## Dependencies

1. Ruby version `ruby-2.4.1`  
2. Rails version `Rails 5.2.1`  
3. Install PostgreSQL  
4. Install [Yarn](https://yarnpkg.com/lang/en/) package manager  

## Steps to run on local  
1. Run `git clone git@bitbucket.org:wonderer_007/stream-viewer.git`  
2. Run `cd stream-viewer`  
3. Run `bundle install`  
4. Run `yarn`  
5. Create ENV file `touch .env` and load your ENVs (check `.env-sample` file for reference)  
6. Run `rails db:create`  
7. Run `rails db:migrate`  
8. Run `rails s` to run the server in `development mode`
9. Visit `http://localhost:3000/` in your favourite browser except IE
10. Happy Configurations ;)

## Assumptions

#### Personas
  I have not taken personas characters seriously For scope of this system I have taken them as 

- Anyone who come to website and login via Google is our **Alex** user.
- Any stream we are watching is of **Natalie** streamer **I do have put search stream function as well so you can search streams of your favourite streamer be it Natalie or Jack**
- **Kevin** is same as **Alex** for us But users cannot paticipate in [SUPER CHAT](https://www.pocket-lint.com/tv/news/youtube/140000-what-is-youtube-s-paid-super-chat-feature-and-how-does-it-work) because they are paid

### Stats Page

  I have not displayed stats on a separate page, rather I have displayed stats on stream page along side live chat to make page more interactive and user can about activites going on in chat rooms while chatting

  **Stats and Chat are updated after every 20 seconds**

### Chat Search

  Instead of  *Alex should be able to search through all messages posted by Natalie's fans' usernames e.g. Kevin* I have implemented simple chat search where user can filter chat messages

### All messages by Kevin to Natalie's livestream chat should be stored in a persistent storage

  I couldn't complete this feature because of one my desin approaches went too buggy that I have to change it. Actaully I implement sent message to livestream via Rails server (through API) and on Rails side I store the chat and hit the Youtube API respectively you can see `app/controllers/api_controller.rb` While I was testing I felt that this is breaking too much then I rushed to hit YouTube API directly resulted in persistent storage


## Design

  Instead of having separate server side and client side I have gone for serving client side from the same server as server side. Rails do provide support for rendering of views on client side via [webpack](https://webpack.js.org/) and [react_on_rails](https://github.com/shakacode/react_on_rails) gems.

### Proof of Concept

  If you go to the (Stream-Viewer)[https://stream-viewer-007.herokuapp.com] open your console and navigate links back and forth you will see it will never make another call to the server.


## Important

  All of the ReactJS code is on path *app/javascript*